<?php
/**
 * Plugin Name: Woocommerce International Phone
 * Plugin URI: https://gitlab.com/rok.sosta/woocommerce-international-phone.git
 * Description: Adds an international phone number prefix and international validation to the checkout billing phone number.
 * Version: 1.0.0
 * Author: Rok Sos
 * Author URI: https://www.e-pik.si
 * Text Domain: woocommerce-international-phone
 * Domain Path: /languages
 * Requires at least: 4.4
 * Tested up to: 4.9
 * License: GPL-3.0+
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

// Include the main WooCommerce class.
if ( ! class_exists( 'WC_Country_Codes' ) ) {
    include_once dirname( __FILE__ ) . '/includes/class-wc-country-codes.php';
}

/**
 * Check if WooCommerce is active
 **/
if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {

    add_action( 'wp', 'init' );

    function init() {

        if ( is_checkout() ) {

            // Scripts
            add_action( 'wp_enqueue_scripts', 'woo_international_scripts' );

            // Checkout fields override
            add_filter( 'woocommerce_checkout_fields' , 'woo_custom_override_checkout_fields', 99 );
        }
    }
}

// Checkout fields override
function woo_custom_override_checkout_fields( $fields ) {

    $fields['billing']['billing_phone_prefix'] = array(
        'label'     => __('Prefix', 'woocommerce'),
        'placeholder'   => _x('Prefix', 'placeholder', 'woocommerce'),
        'type' => 'select',
        'options' => WC_Country_Codes::$country_codes_array,
        'required'  => false,
        'class'     => array('form-row-first phone_prefix'),
        'clear'     => true
    );

    $fields['billing']['billing_email']['class'][0] = 'form-row-wide';
    $fields['billing']['billing_phone_prefix']['priority'] = 100;
    $fields['billing']['billing_phone_prefix']['args']['select2'] = true;
    $fields['billing']['billing_phone']['priority'] = 100;
    $fields['billing']['billing_phone']['class'][0] = 'form-row-last';

    return $fields;
}

// Scripts
function woo_international_scripts() {

    wp_enqueue_style( 'flags', plugin_dir_url( __FILE__ ) . 'assets/css/flags/flags.min.css' );
    wp_enqueue_style( 'woocommerce-international-phone', plugin_dir_url( __FILE__ ) . 'assets/css/woocommerce-international-phone.css' );
    wp_enqueue_script( 'international-phone-compiled', plugin_dir_url( __FILE__ ) . 'assets/js/international-phone-compiled.js', array(), '1.0.0', true );
    wp_enqueue_script( 'select-woo-phone', plugin_dir_url( __FILE__ ) . 'assets/js/select-woo-phone.js', array(), '1.0.0', true );
    wp_localize_script( 'select-woo-phone', 'select_woo_phone_object', array(
        'pluginsUrl' => plugin_dir_url( __FILE__ ),
    ) );
}