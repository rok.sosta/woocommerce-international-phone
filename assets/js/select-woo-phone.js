/* SelectWoo init */
jQuery( document ).ready( function(){

    // Convert and style options with SelectWoo
    var $billing_phone_prefix = jQuery( '#billing_phone_prefix' ).selectWoo({

        templateResult: formatPrefixOptions,
        templateSelection: formatPrefixOptions
    });

    // Change Phone prefix to Country selection
    var $billing_country = jQuery( '#billing_country' );

    if ($billing_country){

        var country_data = $billing_country.select2( 'data' );

        if (country_data){

            $billing_phone_prefix.val( country_data[0].id );
            $billing_phone_prefix.trigger( 'change' );
        }

        // Listen for changes on Country selection
        $billing_country.on( 'select2:select', function (e) {

            var data = e.params.data;

            // Change Phone Prefix to match selected country
            $billing_phone_prefix.val( data.id );
            $billing_phone_prefix.trigger( 'change' );
        });
    }
});

/* Convert and style options with SelectWoo */
function formatPrefixOptions ( state ) {
    if ( !state.id ) {
        return state.text;
    }
    var $state = jQuery(
        '<span ><img sytle="display: inline-block;" src="' + select_woo_phone_object.pluginsUrl + 'assets/css/flags/blank.gif" class="' + state.element.value.toLowerCase() + ' flag flag-' + state.element.value.toLowerCase() + '" /> ' + '<span  class="phone-prefix">' + '(' + state.text + ')' + '</span></span>'
    );
    return $state;
}

/* Phone number validation */
jQuery( document ).ready( function(){

    var $billing_phone = jQuery( '#billing_phone' );

    $billing_phone.focusout( function(){

        var $this     = jQuery( this ),
            $parent   = $this.closest( '.form-row' ),
            validated = phoneNumberParser(),
            message   = 'Phone number is incorrect',
            error     = jQuery( '.woocommerce-phone-error' );

        if ( ! $billing_phone.val() ) {

            error.remove();
            $parent.removeClass( 'woocommerce-invalid validate-phone' ).addClass( 'woocommerce-validated' );

        } else if ( ! validated ) {

            // Remove notice
            error.remove();

            // Display notice
            jQuery( '#billing_phone_field' ).append( '<div class="woocommerce-phone-error">' + message + '</div>' );

            // Add Woocommerce error classes
            $parent.removeClass( 'woocommerce-validated' ).addClass( 'woocommerce-invalid' );

        } else {

            // Input International number
            jQuery( '#billing_phone' ).val( validated );

            error.remove();
            $parent.removeClass( 'woocommerce-invalid validate-phone' ).addClass( 'woocommerce-validated' );
        }
    });
});